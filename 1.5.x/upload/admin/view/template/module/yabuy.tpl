<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb["separator"]; ?><a href="<?php echo $breadcrumb["href"]; ?>"><?php echo $breadcrumb["text"]; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?> 
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
  </div>
  <div class="content">
  
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="form">
	  
        <tr>
          <td><?php echo $entry_status; ?></td>
          <td><select name="yabuy_status">
              <?php if ($yabuy_status) { ?>
              <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
              <option value="0"><?php echo $text_disabled; ?></option>
              <?php } else { ?>
              <option value="1"><?php echo $text_enabled; ?></option>
              <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
              <?php } ?>
            </select></td>
		</tr>

        <tr>
          <td><?php echo $entry_yacompany; ?></td>
          <td><input type="text" name="yabuy_yacompany" value="<?php echo $yabuy_yacompany; ?>" size="10" />
          </td>
		</tr>

        <tr>
          <td><?php echo $entry_token; ?></td>
          <td><input type="text" name="yabuy_token" value="<?php echo $yabuy_token; ?>" size="32" />
          </td>
		</tr>

        <tr>
          <td><?php echo $entry_yalogin; ?></td>
          <td><input type="text" name="yabuy_login" value="<?php echo $yabuy_login; ?>" size="18" />@yandex.ru
          </td>
		</tr>
        <tr>
          <td><?php echo $entry_oauth_token; ?></td>
          <td><input type="text" name="yabuy_oauth_token" value="<?php echo $yabuy_oauth_token; ?>" size="32" /> <a href="javascript:void(0);" id="get_token"><?php echo $text_get; ?></a>
          </td>
		</tr>
		
        <tr>
          <td>Выходные дни:<br><span class="help">Сроки доставки будут увеличены с учетом выходных</span></td>
          <td>
			<input type="checkbox" name="yabuy_weekend_sat" value="1" id="cb_weekend_sat"<?php echo $yabuy_weekend_sat ? ' checked' : ''; ?> /><label for="cb_weekend_sat">Сб.</label>
			<input type="checkbox" name="yabuy_weekend_sun" value="1" id="cb_weekend_sun"<?php echo $yabuy_weekend_sun ? ' checked' : ''; ?> /><label for="cb_weekend_sun">Вс.</label>
          </td>
		</tr>
      </table>
	  <h3><?php echo $entry_modules; ?></h3>
      <table id="modules" class="list">
        <thead>
          <tr>
            <td class="left"><?php echo $text_module_name; ?></td>
            <td class="left"><?php echo $entry_status; ?></td>
            <td class="left"><?php echo $text_module_days; ?><sup><a href="<?php echo $action; ?>#*">**</a></sup></td>
            <td class="left"><?php echo $text_module_before; ?></td>
            <td class="left"><?php echo $text_module_type; ?></td>
            <td><?php echo $entry_payments; ?></td>
          </tr>
        </thead>
        <tbody id="modules-tbody">
        <?php foreach ($modules as $module) { ?>
		  <tr>
			<td class="left"><a href="<?php echo $module['edit_url']; ?>" target="_blank"><?php echo $module['name']; ?></a></td>
			<td class="left"><?php echo $module['status']; ?></td>
			<td class="left">
				<input type="text" name="yabuy_modules[<?php echo $module['code']; ?>][days]" value="<?php echo isset($yabuy_modules[$module['code']]) ? $yabuy_modules[$module['code']]['days'] : ''; ?>" size="6" />
			</td>
			<td class="left">
				<input type="text" name="yabuy_modules[<?php echo $module['code']; ?>][before]" value="<?php echo isset($yabuy_modules[$module['code']]) ? $yabuy_modules[$module['code']]['before'] : ''; ?>" size="6" />
			</td>
			<td class="left">
				<select name="yabuy_modules[<?php echo $module['code']; ?>][type]">
				<option value=""><?php echo $text_module_type_off; ?></option>
				<option value="DELIVERY"<?php echo (isset($yabuy_modules[$module['code']]) && $yabuy_modules[$module['code']]['type'] == 'DELIVERY' ? ' selected="selected"' : ''); ?>><?php echo $text_module_type_delivery; ?></option>
				<option value="POST"<?php echo (isset($yabuy_modules[$module['code']]) && $yabuy_modules[$module['code']]['type'] == 'POST' ? ' selected="selected"' : ''); ?>><?php echo $text_module_type_postal; ?></option>
				</select>
			</td>
            <td><select name="yabuy_modules[<?php echo $module['code']; ?>][payments][]" size="4" multiple>
              <option value="YANDEX"<?php echo (isset($yabuy_modules[$module['code']]) && isset($yabuy_modules[$module['code']]['payments']) && is_array($yabuy_modules[$module['code']]['payments']) && in_array('YANDEX', $yabuy_modules[$module['code']]['payments']) ? ' selected="selected"' : ''); ?>>предоплата через Яндекс</option>
              <option value="SHOP_PREPAID"<?php echo (isset($yabuy_modules[$module['code']]) && isset($yabuy_modules[$module['code']]['payments']) && is_array($yabuy_modules[$module['code']]['payments']) && in_array('SHOP_PREPAID', $yabuy_modules[$module['code']]['payments']) ? ' selected="selected"' : ''); ?>>предоплата магазину</option>
              <option value="CASH_ON_DELIVERY"<?php echo (isset($yabuy_modules[$module['code']]) && isset($yabuy_modules[$module['code']]['payments']) && is_array($yabuy_modules[$module['code']]['payments']) && in_array('CASH_ON_DELIVERY', $yabuy_modules[$module['code']]['payments']) ? ' selected="selected"' : ''); ?>>оплата наличностью при получении заказа</option>
              <option value="CARD_ON_DELIVERY"<?php echo (isset($yabuy_modules[$module['code']]) && isset($yabuy_modules[$module['code']]['payments']) && is_array($yabuy_modules[$module['code']]['payments']) && in_array('CARD_ON_DELIVERY', $yabuy_modules[$module['code']]['payments']) ? ' selected="selected"' : ''); ?>>оплата карточкой при получении заказа</option>
            </select></td>
		  </tr>
		<?php } ?>
        </tbody>
      </table>
      Не все модули доставки можно использовать для "Заказа на Маркете", проверяйте их расчет при помощи тестовых заказов.

	  <h3><?php echo $entry_deliveries; ?></h3>
      <table id="deliveries" class="list">
        <thead>
          <tr>
            <td class="left"><?php echo $text_delivery_id; ?></td>
            <td class="left"><?php echo $text_delivery_name; ?></td>
            <td class="left"><?php echo $text_delivery_price; ?><sup><a href="<?php echo $action; ?>#*">*</a></sup></td>
            <td class="left"><?php echo $text_delivery_days; ?><sup><a href="<?php echo $action; ?>#*">**</a></sup></td>
            <td class="left"><?php echo $text_delivery_before; ?></td>
            <td class="left"><?php echo $text_delivery_region; ?><sup><a href="<?php echo $action; ?>#*">***</a></sup></td>
            <td><?php echo $entry_payments; ?></td>
            <td></td>
          </tr>
        </thead>
        <?php $delivery_row = 0; ?>
        <?php foreach ($yabuy_deliveries as $delivery) { ?>
        <tbody id="delivery-row<?php echo $delivery_row; ?>">
          <tr>
            <td class="left">
				<input type="text" name="yabuy_deliveries[<?php echo $delivery_row; ?>][id]" value="<?php echo $delivery['id']; ?>" size="10" />
            </td>
            <td class="left">
				<input type="text" name="yabuy_deliveries[<?php echo $delivery_row; ?>][name]" value="<?php echo $delivery['name']; ?>" size="35" />
            </td>
            <td class="left">
				<input type="text" name="yabuy_deliveries[<?php echo $delivery_row; ?>][price]" value="<?php echo $delivery['price']; ?>" size="15" />
            </td>
            <td class="left">
				<input type="text" name="yabuy_deliveries[<?php echo $delivery_row; ?>][days]" value="<?php echo $delivery['days']; ?>" size="6" />
            </td>
            <td class="left">
				<input type="text" name="yabuy_deliveries[<?php echo $delivery_row; ?>][before]" value="<?php echo $delivery['before']; ?>" size="6" />
            </td>
            <td class="left">
				<input type="text" name="yabuy_deliveries[<?php echo $delivery_row; ?>][region]" value="<?php echo $delivery['region']; ?>" size="10" />
            </td>
            <td><select name="yabuy_deliveries[<?php echo $delivery_row; ?>][payments][]" size="4" multiple>
              <option value="YANDEX"<?php echo (isset($delivery['payments']) && is_array($delivery['payments']) && in_array('YANDEX', $delivery['payments']) ? ' selected="selected"' : ''); ?>>предоплата через Яндекс</option>
              <option value="SHOP_PREPAID"<?php echo (isset($delivery['payments']) && is_array($delivery['payments']) && in_array('SHOP_PREPAID', $delivery['payments']) ? ' selected="selected"' : ''); ?>>предоплата магазину</option>
              <option value="CASH_ON_DELIVERY"<?php echo (isset($delivery['payments']) && is_array($delivery['payments']) && in_array('CASH_ON_DELIVERY', $delivery['payments']) ? ' selected="selected"' : ''); ?>>оплата наличностью при получении заказа</option>
              <option value="CARD_ON_DELIVERY"<?php echo (isset($delivery['payments']) && is_array($delivery['payments']) && in_array('CARD_ON_DELIVERY', $delivery['payments']) ? ' selected="selected"' : ''); ?>>оплата карточкой при получении заказа</option>
            </select></td>
            <td class="right"><a onclick="$('#delivery-row<?php echo $delivery_row; ?>').remove();" class="button"><span><?php echo $button_remove; ?></span></a></td>
          </tr>
        </tbody>
        <?php $delivery_row++; ?>
        <?php } ?>
		<tfoot>
          <tr>
            <td colspan="7"></td>
            <td class="right"><a onclick="addDelivery();" class="button"><span><?php echo $button_add_outlet; ?></span></a></td>
		  </tr>
		</tfoot>
		</table>

	  <h3><?php echo $entry_postals; ?></h3>
      <table id="postals" class="list">
        <thead>
          <tr>
            <td class="left"><?php echo $text_delivery_id; ?></td>
            <td class="left"><?php echo $text_delivery_name; ?></td>
            <td class="left"><?php echo $text_delivery_price; ?><sup><a href="<?php echo $action; ?>#*">*</a></sup></td>
            <td class="left"><?php echo $text_delivery_days; ?><sup><a href="<?php echo $action; ?>#*">**</a></sup></td>
            <td class="left"><?php echo $text_delivery_before; ?></td>
            <td class="left"><?php echo $text_delivery_region; ?><sup><a href="<?php echo $action; ?>#*">***</a></sup></td>
            <td><?php echo $entry_payments; ?></td>
            <td></td>
          </tr>
        </thead>
        <?php $postal_row = 0; ?>
        <?php foreach ($yabuy_postals as $delivery) { ?>
        <tbody id="postal-row<?php echo $postal_row; ?>">
          <tr>
            <td class="left">
				<input type="text" name="yabuy_postals[<?php echo $postal_row; ?>][id]" value="<?php echo $delivery['id']; ?>" size="10" />
            </td>
            <td class="left">
				<input type="text" name="yabuy_postals[<?php echo $postal_row; ?>][name]" value="<?php echo $delivery['name']; ?>" size="35" />
            </td>
            <td class="left">
				<input type="text" name="yabuy_postals[<?php echo $postal_row; ?>][price]" value="<?php echo $delivery['price']; ?>" size="15" />
            </td>
            <td class="left">
				<input type="text" name="yabuy_postals[<?php echo $postal_row; ?>][days]" value="<?php echo $delivery['days']; ?>" size="6" />
            </td>
            <td class="left">
				<input type="text" name="yabuy_postals[<?php echo $postal_row; ?>][before]" value="<?php echo $delivery['before']; ?>" size="6" />
            </td>
            <td class="left">
				<input type="text" name="yabuy_postals[<?php echo $postal_row; ?>][region]" value="<?php echo $delivery['region']; ?>" size="6" />
            </td>
            <td><select name="yabuy_postals[<?php echo $postal_row; ?>][payments][]" size="4" multiple>
              <option value="YANDEX"<?php echo (isset($delivery['payments']) && is_array($delivery['payments']) && in_array('YANDEX', $delivery['payments']) ? ' selected="selected"' : ''); ?>>предоплата через Яндекс</option>
              <option value="SHOP_PREPAID"<?php echo (isset($delivery['payments']) && is_array($delivery['payments']) && in_array('SHOP_PREPAID', $delivery['payments']) ? ' selected="selected"' : ''); ?>>предоплата магазину</option>
              <option value="CASH_ON_DELIVERY"<?php echo (isset($delivery['payments']) && is_array($delivery['payments']) && in_array('CASH_ON_DELIVERY', $delivery['payments']) ? ' selected="selected"' : ''); ?>>оплата наличностью при получении заказа</option>
              <option value="CARD_ON_DELIVERY"<?php echo (isset($delivery['payments']) && is_array($delivery['payments']) && in_array('CARD_ON_DELIVERY', $delivery['payments']) ? ' selected="selected"' : ''); ?>>оплата карточкой при получении заказа</option>
            </select></td>
            <td class="right"><a onclick="$('#postal-row<?php echo $postal_row; ?>').remove();" class="button"><span><?php echo $button_remove; ?></span></a></td>
          </tr>
        </tbody>
        <?php $postal_row++; ?>
        <?php } ?>
		<tfoot>
          <tr>
            <td colspan="7"></td>
            <td class="right"><a onclick="addPostal();" class="button"><span><?php echo $button_add_outlet; ?></span></a></td>
		  </tr>
		</tfoot>
		</table>
		
	  <h3><?php echo $entry_outlets; ?></h3>
      <table id="outlets" class="list">
        <thead>
          <tr>
            <td class="left"><?php echo $text_outlet_id; ?></td>
            <td class="left"><?php echo $text_outlet_zone; ?></td>
            <td class="left"><?php echo $text_outlet_city; ?></td>
            <td class="left"><?php echo $text_outlet_postcode; ?></td>
            <td class="left"><?php echo $text_outlet_address_1; ?></td>
            <td class="left"><?php echo $text_outlet_address_2; ?></td>
            <td class="left"><?php echo $text_outlet_price; ?><sup><a href="<?php echo $action; ?>#*">*</a></sup></td>
            <td><?php echo $entry_payments; ?></td>
            <td></td>
          </tr>
        </thead>
        <?php $module_row = 0; ?>
        <?php foreach ($yabuy_outlets as $outlet) { ?>
        <tbody id="module-row<?php echo $module_row; ?>">
          <tr>
            <td class="left">
				<input type="text" name="yabuy_outlets[<?php echo $module_row; ?>][id]" value="<?php echo $outlet['id']; ?>" size="5" />
            </td>
            <td class="left">
				<input type="text" name="yabuy_outlets[<?php echo $module_row; ?>][zone]" value="<?php echo $outlet['zone']; ?>" size="25" />
            </td>
            <td class="left">
				<input type="text" name="yabuy_outlets[<?php echo $module_row; ?>][city]" value="<?php echo $outlet['city']; ?>" size="15" />
            </td>
            <td class="left">
				<input type="text" name="yabuy_outlets[<?php echo $module_row; ?>][postcode]" value="<?php echo $outlet['postcode']; ?>" size="6" />
            </td>
            <td class="left">
				<input type="text" name="yabuy_outlets[<?php echo $module_row; ?>][address_1]" value="<?php echo $outlet['address_1']; ?>" size="35" />
            </td>
            <td class="left">
				<input type="text" name="yabuy_outlets[<?php echo $module_row; ?>][address_2]" value="<?php echo $outlet['address_2']; ?>" size="15" />
            </td>
            <td class="left">
				<input type="text" name="yabuy_outlets[<?php echo $module_row; ?>][price]" value="<?php echo (isset($outlet['price']) ? $outlet['price'] : 0); ?>" size="15" />
            </td>
            <td><select name="yabuy_outlets[<?php echo $module_row; ?>][payments][]" size="4" multiple>
              <option value="YANDEX"<?php echo (isset($outlet['payments']) && is_array($outlet['payments']) && in_array('YANDEX', $outlet['payments']) ? ' selected="selected"' : ''); ?>>предоплата через Яндекс</option>
              <option value="SHOP_PREPAID"<?php echo (isset($outlet['payments']) && is_array($outlet['payments']) && in_array('SHOP_PREPAID', $outlet['payments']) ? ' selected="selected"' : ''); ?>>предоплата магазину</option>
              <option value="CASH_ON_DELIVERY"<?php echo (isset($outlet['payments']) && is_array($outlet['payments']) && in_array('CASH_ON_DELIVERY', $outlet['payments']) ? ' selected="selected"' : ''); ?>>оплата наличностью при получении заказа</option>
              <option value="CARD_ON_DELIVERY"<?php echo (isset($outlet['payments']) && is_array($outlet['payments']) && in_array('CARD_ON_DELIVERY', $outlet['payments']) ? ' selected="selected"' : ''); ?>>оплата карточкой при получении заказа</option>
            </select></td>
            <td class="right"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><span><?php echo $button_remove; ?></span></a></td>
          </tr>
        </tbody>
        <?php $module_row++; ?>
        <?php } ?>
        <tfoot id="csvoutlets">
          <tr>
            <td colspan="7"></td>
            <td class="right"><a onclick="addOutlet();" class="button"><span><?php echo $button_add_outlet; ?></span></a></td>
          </tr>
		<?php if ($csvoutlets) { ?>
          <tr>
		  <td colspan="9">
		  <a name="outlets"></a><b><?php echo $text_csvoutlets; ?></b> <a id="load_outlets" href="<?php $action; ?>#outlets" onClick="return loadOutlets();"><?php echo $text_show; ?></a>
		  </td>
		  </tr>
		<?php } ?>
        </tfoot>
      </table>
	  <a name="*"></a>
	  * Цену можно задавать в виде: <b>0:500|3000:200</b> (доставка 500 при сумме заказа от 0, и 200 при сумме заказа от 3000)<br/>
	  ** Срок доставки можно указать как диапазон, например: <b>2-3</b><br/>
	  *** Код региона/города доставки можно узнать на странице управления Яндекс-маркетом (регионы доставки)
	  
    </form>
  </div>
</div>
<script type="text/javascript"><!--
$('#get_token').click(function() {
	var url = 'https://oauth.yandex.ru/authorize?response_type=code&client_id=<?php echo $yandex_oauth_id; ?>&display=popup';
	window.open(url, 'ya_oauth','width=600,height=350,resizable=yes,scrollbars=yes,status=yes,location=false');
	return false;
});

var module_row = <?php echo $module_row; ?>;
var delivery_row = <?php echo $delivery_row; ?>;
var postal_row = <?php echo $postal_row; ?>;

function loadOutlets() {
	$('#load_outlets').fadeOut();
	$.ajax({
		url: '<?php echo $url_csvoutlets; ?>&token=<?php echo $token; ?>',
		dataType: 'json',
		success: function(json) {
			for (i = 0; i < json.length; i++) {
				var outlet = json[i];
				html  = '  <tr>';
				html += '    <td class="left">' +outlet.id+ '</td>';
				html += '    <td class="left">' +outlet.zone+ '</td>';
				html += '    <td class="left">' +outlet.city+ '</td>';
				html += '    <td class="left">' +outlet.postcode+ '</td>';
				html += '    <td class="left">' +outlet.address_1+ '</td>';
				html += '    <td class="left">' +outlet.address_2+ '</td>';
				html += '    <td class="left">' + (outlet.price ? outlet.price : 0) + '</td>';
				html += '    <td class="left">' + (outlet.payment ? outlet.payment : 'CASH_ON_DELIVERY') + '</td>';
				html += '    <td class="right">&nbsp;</td>';
				html += '  </tr>';
				$('#csvoutlets').append(html);
			}
		}
	})
	return false;
}

function addDelivery() {
	html  = '<tbody id="delivery-row' + delivery_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><input type="text" name="yabuy_deliveries[' + delivery_row + '][id]" value="" size="5" /></td>';
	html += '    <td class="left"><input type="text" name="yabuy_deliveries[' + delivery_row + '][name]" value="" size="35" /></td>';
	html += '    <td class="left"><input type="text" name="yabuy_deliveries[' + delivery_row + '][price]" value="0" size="15" /></td>';
	html += '    <td class="left"><input type="text" name="yabuy_deliveries[' + delivery_row + '][days]" value="" size="6" /></td>';
	html += '    <td class="left"><input type="text" name="yabuy_deliveries[' + delivery_row + '][before]" value="0" size="6" /></td>';
	html += '    <td class="left"><input type="text" name="yabuy_deliveries[' + delivery_row + '][region]" value="" size="10" /></td>';
	
	html += '    <td><select name="yabuy_deliveries[' + delivery_row + '][payments][]" size="4" multiple>';
	html += '    <option value="YANDEX" selected>предоплата через Яндекс</option>';
	html += '    <option value="SHOP_PREPAID">предоплата магазину</option>';
	html += '    <option value="CASH_ON_DELIVERY">оплата наличностью при получении заказа</option>';
	html += '    <option value="CARD_ON_DELIVERY">оплата карточкой при получении заказа</option>';
	html += '    </select></td>';
	
	html += '    <td class="right"><a onclick="$(\'#delivery-row' + delivery_row + '\').remove();" class="button"><span><?php echo $button_remove; ?></span></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	$('#deliveries tfoot').before(html);
	
	delivery_row++;
}

function addPostal() {
	html  = '<tbody id="postal-row' + postal_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><input type="text" name="yabuy_postals[' + postal_row + '][id]" value="" size="5" /></td>';
	html += '    <td class="left"><input type="text" name="yabuy_postals[' + postal_row + '][name]" value="" size="35" /></td>';
	html += '    <td class="left"><input type="text" name="yabuy_postals[' + postal_row + '][price]" value="0" size="15" /></td>';
	html += '    <td class="left"><input type="text" name="yabuy_postals[' + postal_row + '][days]" value="" size="6" /></td>';
	html += '    <td class="left"><input type="text" name="yabuy_postals[' + postal_row + '][before]" value="" size="6" /></td>';
	html += '    <td class="left"><input type="text" name="yabuy_postals[' + postal_row + '][region]" value="" size="10" /></td>';
	
	html += '    <td><select name="yabuy_postals[' + postal_row + '][payments][]" size="4" multiple>';
	html += '    <option value="YANDEX" selected>предоплата через Яндекс</option>';
	html += '    <option value="SHOP_PREPAID">предоплата магазину</option>';
	html += '    <option value="CASH_ON_DELIVERY">оплата наличностью при получении заказа</option>';
	html += '    <option value="CARD_ON_DELIVERY">оплата карточкой при получении заказа</option>';
	html += '    </select></td>';
	
	html += '    <td class="right"><a onclick="$(\'#delivery-row' + postal_row + '\').remove();" class="button"><span><?php echo $button_remove; ?></span></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	$('#postals tfoot').before(html);
	
	postal_row++;
}

function addOutlet() {
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><input type="text" name="yabuy_outlets[' + module_row + '][id]" value="" size="7" /></td>';
	html += '    <td class="left"><input type="text" name="yabuy_outlets[' + module_row + '][zone]" value="" size="25" /></td>';
	html += '    <td class="left"><input type="text" name="yabuy_outlets[' + module_row + '][city]" value="" size="15" /></td>';
	html += '    <td class="left"><input type="text" name="yabuy_outlets[' + module_row + '][postcode]" value="" size="6" /></td>';
	html += '    <td class="left"><input type="text" name="yabuy_outlets[' + module_row + '][address_1]" value="" size="35" /></td>';
	html += '    <td class="left"><input type="text" name="yabuy_outlets[' + module_row + '][address_2]" value="" size="15" /></td>';
	html += '    <td class="left"><input type="text" name="yabuy_outlets[' + module_row + '][price]" value="0" size="15" /></td>';
	
	html += '    <td><select name="yabuy_outlets[' + module_row + '][payments][]" size="4" multiple>';
	html += '    <option value="YANDEX" selected>предоплата через Яндекс</option>';
	html += '    <option value="SHOP_PREPAID">предоплата магазину</option>';
	html += '    <option value="CASH_ON_DELIVERY">оплата наличностью при получении заказа</option>';
	html += '    <option value="CARD_ON_DELIVERY">оплата карточкой при получении заказа</option>';
	html += '    </select></td>';
	
	html += '    <td class="right"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><span><?php echo $button_remove; ?></span></a></td>';
	
	html += '  </tr>';
	html += '</tbody>';
	
	$('#outlets tfoot').before(html);
	
	module_row++;
}
//--></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-42296537-2']);
  _gaq.push(['_setDomainName', 'sourcedistillery.com']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<?php echo $footer; ?>
