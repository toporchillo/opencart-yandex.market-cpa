<?php
// Heading 
$_["heading_title"]  = 'Яндекс CPA «Заказ на Маркете»'; 

// Text
$_["text_module"]            = "Модули";

$_["text_module_name"] = "Название модуля доставки";
$_["text_module_days"] = "Срок доставки (дней)";
$_["text_module_before"] = "Час &quot;перескока&quot;";
$_["text_module_type"] = "Тип доставки";
$_["text_module_type_off"] = "Не использовать";
$_["text_module_type_delivery"] = "Курьерская доставка";
$_["text_module_type_postal"] = "Почтовая доставка";

$_["text_delivery_id"]		= "ID";
$_["text_delivery_name"]	= "Название";
$_["text_delivery_price"]	= "Стоимость, руб.";
$_["text_delivery_days"]	= "Срок доставки (дней)";
$_["text_delivery_before"]	= "Час &quot;перескока&quot;";
$_["text_delivery_region"]	= "Яндекс-код региона";

$_["text_outlet_id"]         = "Яндекс ID";
$_["text_outlet_price"]      = "Стоимость самовывоза";
$_["text_outlet_zone"]       = "Регион";
$_["text_outlet_city"]       = "Город";
$_["text_outlet_postcode"]   = "Почтовый индекс";
$_["text_outlet_address_1"]  = "Адрес";
$_["text_outlet_address_2"]  = "Адрес (продолжение)";
$_["text_outlet_price"]		 = "Стоимость самовывоза";

$_["text_csvoutlets"]  = 'Также пункты самовывоза загружаются из csv-файла %s';
$_["text_show"]  = 'Показать';
$_["text_get"]  = 'Получить';

$_["text_success"]	= 'Модуль Yandex CPA "Покупка на Маркете" успешно обновлен!';

// Entry
$_["entry_status"]       = "Статус:";
$_["entry_yacompany"]    = "ID магазина в Яндекс.Маркете:";
$_["entry_yalogin"]		 = "Яндекс-логин администратора магазина:";
$_["entry_token"]        = "Авторизационный токен:";
$_["entry_oauth_token"]  = 'Токен oAuth:<br><span class="help">Для смены статуса заказа</span>';
$_["entry_modules"]     = "Модули доставки";
$_["entry_payments"]     = "Способы оплаты";
$_["entry_deliveries"]   = "Курьерская доставка";
$_["entry_postals"]   	 = "Доставка почтой, EMS и т.п.";
$_["entry_outlets"]      = "Точки продаж (пункты самовывоза)";

$_["button_add_outlet"]  = "Добавить";
$_["button_remove"]  = "Удалить";

// Error
$_["error_permission"]    = 'У Вас нет прав для изменения модуля Yandex CPA "Покупка на Маркете"!';
